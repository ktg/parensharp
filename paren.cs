﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;

namespace ParenSharp {
    public class paren {
        internal const string VERSION = "0.3.3";

        internal paren() {
            init();
        }

        internal class node : ICloneable {
            internal object value;
            internal Type clazz = null; // type hint

            internal node() {
            }
            internal node(object value) {
                this.value = value;
            }
            public object Clone() {
                node r = new node(this.value);
                r.clazz = this.clazz;
                return r;
            }
            internal int intValue() {                
                return Convert.ToInt32(value);
            }
            internal double doubleValue() {                
                return Convert.ToDouble(value);
            }
            internal long longValue() {
                return Convert.ToInt64(value);
            }
            internal bool booleanValue() {
                return Convert.ToBoolean(value);
            }
            internal string stringValue() {
                if (value == null) {
                    return "";
                }
                if (value is List<node>) {
                    return ListToString((List<node>)value);
                }
                return value.ToString();
            }

            internal List<node> arrayListValue() {
                return (List<node>)value;
            }

            internal string type() {
                if (value == null) {
                    return "null";
                }
                else {
                    return value.GetType().FullName;                    
                }
            }

            public override string ToString() {
                return stringValue();
            }

            public static explicit operator int(node n) {
                return Convert.ToInt32(n.value);
            }

            public static explicit operator long(node n) {
                return Convert.ToInt64(n.value);
            }

            public static explicit operator double(node n) {
                return Convert.ToDouble(n.value);
            }

            public static explicit operator bool(node n) {
                return Convert.ToBoolean(n.value);
            }
        }

        // frequently used constants
        internal static readonly node node_true = new node(true);
        internal static readonly node node_false = new node(false);
        internal static readonly node node_0 = new node(0);
        internal static readonly node node_1 = new node(1);
        internal static readonly node node_null = new node();
        node node_quote;

        internal class symbol {
            public static Dictionary<string, int> symcode = new Dictionary<string, int>();
            public static List<string> symname = new List<string>();
            //internal string name;
            public int code;
            public static int ToCode(string name) {
                int r;
                if (!symcode.TryGetValue(name, out r)) {
                    r = symcode.Count;
                    symcode[name] = r;
                    symname.Add(name);
                }
                return r;
            }

            public symbol(string name) {
                code = ToCode(name);
            }

            public override string ToString() {
                return symname[code];
            }
        }

        internal class fn { // anonymous function
            internal List<node> def; // definition
            internal environment env;
            internal fn(List<node> def, environment env) {
                this.def = def;
                this.env = env;
            }
            public override string ToString() {
                return ListToString(def);
            }
        }

        static string ListToString(List<node> lst) {
            return "(" + string.Join(" ", lst) + ")";
        }

        internal class environment {
            //internal Dictionary<string, node> env = new Dictionary<string, node>();
            internal Dictionary<int, node> env = new Dictionary<int, node>();
            internal environment outer;
            internal environment() {
                this.outer = null;
            }
            internal environment(environment outer) {
                this.outer = outer;
            }
            internal node get(int code) {
                node found;
                if (env.TryGetValue(code, out found))
                    return found;
                else {
                    if (outer != null) {
                        return outer.get(code);
                    }
                    else {
                        return null;
                    }
                }
            }

            internal node set(int code, node v) {
                return env[code] = (node)v.Clone();
            }
        }

        private class tokenizer {
            internal List<string> ret = new List<string>();
            internal string acc = ""; // accumulator
            internal string s;
            public int unclosed = 0;

            public tokenizer(string s) {
                this.s = s;
            }

            internal void emit() {
                if (acc.Length > 0) {
                    ret.Add(acc);
                    acc = "";
                }
            }

            public List<string> tokenize() {
                int last = s.Length - 1;
                for (int pos = 0; pos <= last; pos++) {
                    char c = s[pos];
                    if (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
                        emit();
                    }
                    else if (c == ';' || c == '#') { // end-of-line comment
                        emit();
                        do {
                            pos++;
                        } while (pos <= last && s[pos] != '\n');
                    }
                    else if (c == '"') { // beginning of string
                        unclosed++;
                        emit();
                        acc += '"';
                        pos++;
                        while (pos <= last) {
                            if (s[pos] == '"') {
                                unclosed--;
                                break;
                            }
                            if (s[pos] == '\\') { // escape
                                char next = s[pos + 1];
                                if (next == 'r') {
                                    next = '\r';
                                }
                                else if (next == 'n') {
                                    next = '\n';
                                }
                                else if (next == 't') {
                                    next = '\t';
                                }
                                acc += next;
                                pos += 2;
                            }
                            else {
                                acc += s[pos];
                                pos++;
                            }
                        }
                        emit();
                    }
                    else if (c == '(') {
                        unclosed++;
                        emit();
                        acc += c;
                        emit();
                    }
                    else if (c == ')') {
                        unclosed--;
                        emit();
                        acc += c;
                        emit();
                    }
                    else {
                        acc += c;
                    }
                }
                emit();
                return ret;
            }
        }

        public static List<string> tokenize(string s) {
            return (new tokenizer(s)).tokenize();
        }

        private class parser {
            internal int pos = 0;
            internal List<string> tokens;
            public parser(List<string> tokens) {
                this.tokens = tokens;
            }
            public List<node> parse() {
                List<node> ret = new List<node>();
                int last = tokens.Count - 1;
                for (; pos <= last; pos++) {
                    string tok = tokens[pos];
                    if (tok[0] == '"') { // double-quoted string
                        ret.Add(new node(tok.Substring(1)));
                    }
                    else if (tok.Equals("(")) { // list
                        pos++;
                        ret.Add(new node(parse()));
                    }
                    else if (tok.Equals(")")) { // end of list
                        break;
                    }
                    else if (char.IsDigit(tok[0]) || tok[0] == '-' && tok.Length >= 2 && char.IsDigit(tok[1])) { // number
                        if (tok.IndexOf('.') != -1 || tok.IndexOf('e') != -1) { // double
                            ret.Add(new node(Convert.ToDouble(tok)));
                        }
                        else if (tok.EndsWith("L") || tok.EndsWith("l")) { // long
                            ret.Add(new node(Convert.ToInt64(tok.Substring(0, tok.Length - 1))));
                        }
                        else {
                            ret.Add(new node(Convert.ToInt32(tok)));
                        }
                    }
                    else { // symbol
                        ret.Add(new node(new symbol(tok)));
                    }
                }
                return ret;
            }
        }

        internal List<node> parse(string s) {
            return (new parser(tokenize(s))).parse();
        }

        internal enum builtin { PLUS, MINUS, MUL, DIV, CARET, PERCENT, SQRT, INC, DEC, PLUSPLUS, MINUSMINUS, FLOOR, CEIL, LN, LOG10, RAND, EQ, EQEQ, NOTEQ, LT, GT, LTE, GTE, ANDAND, OROR, NOT, IF, WHEN, FOR, WHILE, STRLEN, STRCAT, CHAR_AT, CHR, INT, DOUBLE, STRING, READ_STRING, TYPE, SET, EVAL, QUOTE, FN, LIST, APPLY, FOLD, MAP, FILTER, RANGE, NTH, LENGTH, BEGIN, DOT, DOTGET, DOTSET, NEW, PR, PRN, EXIT, SYSTEM, CONS, LONG, NULLP, CAST, DEFMACRO, READ_LINE, SLURP, SPIT, THREAD, DEF }

        internal environment global_env = new environment(); // variables. compile-time

        internal void print_logo() {
            Console.WriteLine("Paren# " + VERSION + " (C) 2013-2015 Kim, Taegyoon");
            Console.WriteLine("Predefined Symbols:");
            foreach (var x in new SortedSet<string>(global_env.env.Keys.ToList().Select(x => symbol.symname[x]))) {
                Console.Write(" " + x);
            }
            Console.WriteLine();

            Console.WriteLine("Macros:");
            foreach (var key in new SortedSet<string>(macros.Keys)) {
                Console.Write(" " + key);
            }
            Console.WriteLine();
        }

        internal void init() {
            global_env.env[symbol.ToCode("true")] = node_true;
            global_env.env[symbol.ToCode("false")] = node_false;
            global_env.env[symbol.ToCode("E")] = new node(2.71828182845904523536);
            global_env.env[symbol.ToCode("PI")] = new node(3.14159265358979323846);
            global_env.env[symbol.ToCode("null")] = new node();
            global_env.env[symbol.ToCode("+")] = new node(builtin.PLUS);
            global_env.env[symbol.ToCode("-")] = new node(builtin.MINUS);
            global_env.env[symbol.ToCode("*")] = new node(builtin.MUL);
            global_env.env[symbol.ToCode("/")] = new node(builtin.DIV);
            global_env.env[symbol.ToCode("^")] = new node(builtin.CARET);
            global_env.env[symbol.ToCode("%")] = new node(builtin.PERCENT);
            global_env.env[symbol.ToCode("sqrt")] = new node(builtin.SQRT);
            global_env.env[symbol.ToCode("inc")] = new node(builtin.INC);
            global_env.env[symbol.ToCode("dec")] = new node(builtin.DEC);
            global_env.env[symbol.ToCode("++")] = new node(builtin.PLUSPLUS);
            global_env.env[symbol.ToCode("--")] = new node(builtin.MINUSMINUS);
            global_env.env[symbol.ToCode("floor")] = new node(builtin.FLOOR);
            global_env.env[symbol.ToCode("ceil")] = new node(builtin.CEIL);
            global_env.env[symbol.ToCode("ln")] = new node(builtin.LN);
            global_env.env[symbol.ToCode("log10")] = new node(builtin.LOG10);
            global_env.env[symbol.ToCode("rand")] = new node(builtin.RAND);
            global_env.env[symbol.ToCode("=")] = new node(builtin.EQ);
            global_env.env[symbol.ToCode("==")] = new node(builtin.EQEQ);
            global_env.env[symbol.ToCode("!=")] = new node(builtin.NOTEQ);
            global_env.env[symbol.ToCode("<")] = new node(builtin.LT);
            global_env.env[symbol.ToCode(">")] = new node(builtin.GT);
            global_env.env[symbol.ToCode("<=")] = new node(builtin.LTE);
            global_env.env[symbol.ToCode(">=")] = new node(builtin.GTE);
            global_env.env[symbol.ToCode("&&")] = new node(builtin.ANDAND);
            global_env.env[symbol.ToCode("||")] = new node(builtin.OROR);
            global_env.env[symbol.ToCode("!")] = new node(builtin.NOT);
            global_env.env[symbol.ToCode("if")] = new node(builtin.IF);
            global_env.env[symbol.ToCode("when")] = new node(builtin.WHEN);
            global_env.env[symbol.ToCode("for")] = new node(builtin.FOR);
            global_env.env[symbol.ToCode("while")] = new node(builtin.WHILE);
            global_env.env[symbol.ToCode("strlen")] = new node(builtin.STRLEN);
            global_env.env[symbol.ToCode("strcat")] = new node(builtin.STRCAT);
            global_env.env[symbol.ToCode("char-at")] = new node(builtin.CHAR_AT);
            global_env.env[symbol.ToCode("chr")] = new node(builtin.CHR);
            global_env.env[symbol.ToCode("int")] = new node(builtin.INT);
            global_env.env[symbol.ToCode("double")] = new node(builtin.DOUBLE);
            global_env.env[symbol.ToCode("string")] = new node(builtin.STRING);
            global_env.env[symbol.ToCode("read-string")] = new node(builtin.READ_STRING);
            global_env.env[symbol.ToCode("type")] = new node(builtin.TYPE);
            global_env.env[symbol.ToCode("eval")] = new node(builtin.EVAL);
            global_env.env[symbol.ToCode("quote")] = new node(builtin.QUOTE);
            global_env.env[symbol.ToCode("fn")] = new node(builtin.FN);
            global_env.env[symbol.ToCode("list")] = new node(builtin.LIST);
            global_env.env[symbol.ToCode("apply")] = new node(builtin.APPLY);
            global_env.env[symbol.ToCode("fold")] = new node(builtin.FOLD);
            global_env.env[symbol.ToCode("map")] = new node(builtin.MAP);
            global_env.env[symbol.ToCode("filter")] = new node(builtin.FILTER);
            global_env.env[symbol.ToCode("range")] = new node(builtin.RANGE);
            global_env.env[symbol.ToCode("nth")] = new node(builtin.NTH);
            global_env.env[symbol.ToCode("length")] = new node(builtin.LENGTH);
            global_env.env[symbol.ToCode("begin")] = new node(builtin.BEGIN);
            global_env.env[symbol.ToCode(".")] = new node(builtin.DOT);
            global_env.env[symbol.ToCode(".get")] = new node(builtin.DOTGET);
            global_env.env[symbol.ToCode(".set")] = new node(builtin.DOTSET);
            global_env.env[symbol.ToCode("new")] = new node(builtin.NEW);
            global_env.env[symbol.ToCode("set")] = new node(builtin.SET);
            global_env.env[symbol.ToCode("pr")] = new node(builtin.PR);
            global_env.env[symbol.ToCode("prn")] = new node(builtin.PRN);
            global_env.env[symbol.ToCode("exit")] = new node(builtin.EXIT);
            global_env.env[symbol.ToCode("system")] = new node(builtin.SYSTEM);
            global_env.env[symbol.ToCode("cons")] = new node(builtin.CONS);
            global_env.env[symbol.ToCode("long")] = new node(builtin.LONG);
            global_env.env[symbol.ToCode("null?")] = new node(builtin.NULLP);
            global_env.env[symbol.ToCode("cast")] = new node(builtin.CAST);
            global_env.env[symbol.ToCode("defmacro")] = new node(builtin.DEFMACRO);
            global_env.env[symbol.ToCode("read-line")] = new node(builtin.READ_LINE);
            global_env.env[symbol.ToCode("slurp")] = new node(builtin.SLURP);
            global_env.env[symbol.ToCode("spit")] = new node(builtin.SPIT);
            global_env.env[symbol.ToCode("thread")] = new node(builtin.THREAD);
            global_env.env[symbol.ToCode("def")] = new node(builtin.DEF);
            node_quote = global_env.get(symbol.ToCode("quote"));
            eval_string("(defmacro setfn (name ...) (set name (fn ...)))");            
            eval_string("(defmacro defn (name ...) (def name (fn ...)))");
            eval_string("(defmacro join (t) (. t Join))");
        }
        internal Dictionary<string, node[]> macros = new Dictionary<string, node[]>();
        internal node apply_macro(node body, Dictionary<string, node> vars) {
            if (body.value is List<node>) {
                List<node> bvec = (List<node>)body.value;
                List<node> ret = new List<node>();
                for (int i = 0; i < bvec.Count; i++) {
                    node b = bvec[i];
                    if (b.stringValue().Equals("...")) {
                        ret.AddRange(vars[b.stringValue()].arrayListValue());
                    }
                    else {
                        ret.Add(apply_macro(bvec[i], vars));
                    }
                }
                return new node(ret);
            }
            else {
                string bstr = body.stringValue();
                if (vars.ContainsKey(bstr)) {
                    return vars[bstr];
                }
                else {
                    return body;
                }
            }
        }
        internal node macroexpand(node n) {
            List<node> nArrayList = n.arrayListValue();
            if (macros.ContainsKey(nArrayList[0].stringValue())) {
                node[] macro = macros[nArrayList[0].stringValue()];
                Dictionary<string, node> macrovars = new Dictionary<string, node>();
                List<node> argsyms = macro[0].arrayListValue();
                for (int i = 0; i < argsyms.Count; i++) {
                    string argsym = argsyms[i].stringValue();
                    if (argsym.Equals("...")) {
                        node n2 = new node(new List<node>());
                        macrovars[argsym] = n2;
                        List<node> ellipsis = n2.arrayListValue();
                        for (int i2 = i + 1; i2 < nArrayList.Count; i2++) {
                            ellipsis.Add(nArrayList[i2]);
                        }
                        break;
                    }
                    else {
                        macrovars[argsyms[i].stringValue()] = nArrayList[i + 1];
                    }
                }
                return apply_macro(macro[1], macrovars);
            }
            else {
                return n;
            }
        }

        node quote(node n) {
            List<node> r = new List<node>();
            r.Add(node_quote);
            r.Add(n);
            return new node(r);
        }

        internal node compile(node n) {
            if (n.value is List<node>) { // function (FUNCTION ARGUMENT ..)
                List<node> nArrayList = n.arrayListValue();
                if (nArrayList.Count == 0) {
                    return n;
                }
                node func = compile(nArrayList[0]);
                if (func.value is symbol && func.ToString() == "defmacro") {
                    // (defmacro add (a b) (+ a b)) ; define macro
                    macros[nArrayList[1].stringValue()] = new node[] { nArrayList[2], nArrayList[3] };
                    return node_null;
                } else {
                    if (macros.ContainsKey(nArrayList[0].stringValue())) { // compile macro
                        return compile(macroexpand(n));
                    }
                    else {
                        List<node> r = new List<node>();
                        foreach (node n2 in nArrayList) {
                            r.Add(compile(n2));
                        }
                        return new node(r);
                    }
                }
            }
            else {
                return n;
            }
        }

        internal node eval(node n, environment env) {
    	    if (n.value is symbol) {
                node r = env.get(((symbol)n.value).code);
                //if (r == null) {
                //    Console.Error.WriteLine("Unknown variable: " + n.ToString());
                //    return node_null;
                //}
    		    return r;
    	    }
            else if (n.value is List<node>) { // function (FUNCTION ARGUMENT ..)
                List<node> nArrayList = n.arrayListValue();
                if (nArrayList.Count == 0) {
                    return node_null;
                }
                node func = eval(nArrayList[0], env);
                builtin foundBuiltin;
                if (func.value is builtin) {
                    foundBuiltin = (builtin)func.value;
                    switch (foundBuiltin) {
                        case paren.builtin.PLUS: { // (+ X ..)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_0;
                                }
                                node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    int acc = first.intValue();
                                    for (int i = 2; i < len; i++) {
                                        acc += eval(nArrayList[i], env).intValue();
                                    }
                                    return new node(acc);
                                }
                                else if (first.value is long) {
                                    long acc = first.longValue();
                                    for (int i = 2; i < len; i++) {
                                        acc += eval(nArrayList[i], env).longValue();
                                    }
                                    return new node(acc);
                                }
                                else {
                                    double acc = first.doubleValue();
                                    for (int i = 2; i < len; i++) {
                                        acc += eval(nArrayList[i], env).doubleValue();
                                    }
                                    return new node(acc);
                                }
                            }
                        case paren.builtin.MINUS: { // (- X ..)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_0;
                                }
                                node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    int acc = (int)first;
                                    for (int i = 2; i < len; i++) {
                                        acc -= (int)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                                else if (first.value is long) {
                                    long acc = (long)first;
                                    for (int i = 2; i < len; i++) {
                                        acc -= (long)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                                else {
                                    double acc = (double)first;
                                    for (int i = 2; i < len; i++) {
                                        acc -= (double)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                            }
                        case paren.builtin.MUL: { // (* X ..)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_1;
                                }
                                node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    int acc = (int)first;
                                    for (int i = 2; i < len; i++) {
                                        acc *= (int)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                                else if (first.value is long) {
                                    long acc = (long)first;
                                    for (int i = 2; i < len; i++) {
                                        acc *= (long)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                                else {
                                    double acc = (double)first;
                                    for (int i = 2; i < len; i++) {
                                        acc *= (double)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                            }
                        case paren.builtin.DIV: { // (/ X ..)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_1;
                                }
                                node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    int acc = (int)first;
                                    for (int i = 2; i < len; i++) {
                                        acc /= (int)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                                else if (first.value is long) {
                                    long acc = (long)first;
                                    for (int i = 2; i < len; i++) {
                                        acc /= (long)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                                else {
                                    double acc = (double)first;
                                    for (int i = 2; i < len; i++) {
                                        acc /= (double)eval(nArrayList[i], env);
                                    }
                                    return new node(acc);
                                }
                            }
                        case paren.builtin.CARET: { // (^ BASE EXPONENT)
                            return new node(Math.Pow((double)eval(nArrayList[1], env), (double)eval(nArrayList[2], env)));
                            }
                        case paren.builtin.PERCENT: { // (% DIVIDEND DIVISOR)
                            return new node((int)eval(nArrayList[1], env) % (int)eval(nArrayList[2], env));
                            }
                        case paren.builtin.SQRT: { // (sqrt X)
                            return new node(Math.Sqrt((double)eval(nArrayList[1], env)));
                            }
                        case paren.builtin.INC: { // (inc X)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_0;
                                }
                                node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    return new node((int)first + 1);
                                }
                                else if (first.value is long) {
                                    return new node((long)first + 1);
                                }
                                else {
                                    return new node((double)first + 1.0);
                                }
                            }
                        case paren.builtin.DEC: { // (dec X)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_0;
                                }
                                node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    return new node((int)first - 1);
                                }
                                else if (first.value is long) {
                                    return new node((long)first - 1);
                                }
                                else {
                                    return new node((double)first - 1.0);
                                }
                            }
                        case paren.builtin.PLUSPLUS: { // (++ X)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_0;
                                }
                                //node n2 = nArrayList[1];
                                //node n2 = env.get(nArrayList[1].ToString());
                                node n2 = env.get(((symbol)nArrayList[1].value).code);
                                if (n2.value is int) {
                                    n2.value = (int)n2 + 1;
                                }
                                else if (n2.value is long) {
                                    n2.value = (long)n2 + 1;
                                }
                                else {
                                    n2.value = (double)n2 + 1.0;
                                }
                                return n2;
                            }
                        case paren.builtin.MINUSMINUS: { // (-- X)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return node_0;
                                }
                                //node n2 = nArrayList[1];
                                //node n2 = env.get(nArrayList[1].ToString());
                                node n2 = env.get(((symbol)nArrayList[1].value).code);
                                if (n2.value is int) {
                                    n2.value = (int)n2 - 1;
                                }
                                else if (n2.value is long) {
                                    n2.value = (long)n2 - 1;
                                }
                                else {
                                    n2.value = ((double)n2 - 1.0);
                                }
                                return n2;
                            }
                        case paren.builtin.FLOOR: { // (floor X)
                            return new node(Math.Floor((double)eval(nArrayList[1], env)));
                            }
                        case paren.builtin.CEIL: { // (ceil X)
                            return new node(Math.Ceiling((double)eval(nArrayList[1], env)));
                            }
                        case paren.builtin.LN: { // (ln X)
                            return new node(Math.Log((double)eval(nArrayList[1], env)));
                            }
                        case paren.builtin.LOG10: { // (log10 X)
                            return new node(Math.Log10((double)eval(nArrayList[1], env)));
                            }
                        case paren.builtin.RAND: { // (rand)                                
                                return new node(new Random().NextDouble());
                            }
                        case paren.builtin.SET: { // (set SYMBOL-OR-PLACE VALUE)
                            node var = eval(nArrayList[1], env);
                            node value = eval(nArrayList[2], env);
                            if (var == null) // new variable
                                return env.set(((symbol)nArrayList[1].value).code, value);
                            else {
                                var.value = value.value;
                                return var;
                            }
                        }
                        case paren.builtin.DEF: // (def SYMBOL VALUE) ; set in the current environment
                            {
                                node value = eval(nArrayList[2], env);
                                return env.set(((symbol)nArrayList[1].value).code, value);
                            }
                        case paren.builtin.EQ: { // (= X ..) short-circuit, Object.equals()
                            node first = eval(nArrayList[1], env);
                                object firstv = first.value;
                                for (int i = 2; i < nArrayList.Count; i++) {
                                    if (!eval(nArrayList[i], env).value.Equals(firstv)) {
                                        return node_false;
                                    }
                                }
                                return node_true;
                            }
                        case paren.builtin.EQEQ: { // (== X ..) short-circuit
                            node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    int firstv = (int)first;
                                    for (int i = 2; i < nArrayList.Count; i++) {
                                        if ((int)eval(nArrayList[i], env) != firstv) {
                                            return node_false;
                                        }
                                    }
                                }
                                else if (first.value is long) {
                                    long firstv = (long)first;
                                    for (int i = 2; i < nArrayList.Count; i++) {
                                        if ((long)eval(nArrayList[i], env) != firstv) {
                                            return node_false;
                                        }
                                    }
                                }
                                else {
                                    double firstv = (double)first;
                                    for (int i = 2; i < nArrayList.Count; i++) {
                                        if ((double)eval(nArrayList[i], env) != firstv) {
                                            return node_false;
                                        }
                                    }
                                }
                                return node_true;
                            }
                        case paren.builtin.NOTEQ: { // (!= X ..) short-circuit
                            node first = eval(nArrayList[1], env);
                                if (first.value is int) {
                                    int firstv = (int)first;
                                    for (int i = 2; i < nArrayList.Count; i++) {
                                        if ((int)eval(nArrayList[i], env) == firstv) {
                                            return node_false;
                                        }
                                    }
                                }
                                else if (first.value is long) {
                                    long firstv = (long)first;
                                    for (int i = 2; i < nArrayList.Count; i++) {
                                        if ((long)eval(nArrayList[i], env) == firstv) {
                                            return node_false;
                                        }
                                    }
                                }
                                else {
                                    double firstv = (double)first;
                                    for (int i = 2; i < nArrayList.Count; i++) {
                                        if ((double)eval(nArrayList[i], env) == firstv) {
                                            return node_false;
                                        }
                                    }
                                }
                                return node_true;
                            }
                        case paren.builtin.LT: { // (< X Y)
                            node first = eval(nArrayList[1], env);
                            node second = eval(nArrayList[2], env);
                                if (first.value is int) {
                                    return new node((int)first < (int)second);
                                }
                                else if (first.value is long) {
                                    return new node((long)first < (long)second);
                                }
                                else {
                                    return new node((double)first < (double)second);
                                }
                            }
                        case paren.builtin.GT: { // (> X Y)
                            node first = eval(nArrayList[1], env);
                            node second = eval(nArrayList[2], env);
                                if (first.value is int) {
                                    return new node((int)first > (int)second);
                                }
                                else if (first.value is long) {
                                    return new node((long)first > (long)second);
                                }
                                else {
                                    return new node((double)first > (double)second);
                                }
                            }
                        case paren.builtin.LTE: { // (<= X Y)
                            node first = eval(nArrayList[1], env);
                            node second = eval(nArrayList[2], env);
                                if (first.value is int) {
                                    return new node((int)first <= (int)second);
                                }
                                else if (first.value is long) {
                                    return new node((long)first <= (long)second);
                                }
                                else {
                                    return new node((double)first <= (double)second);
                                }
                            }
                        case paren.builtin.GTE: { // (>= X Y)
                            node first = eval(nArrayList[1], env);
                            node second = eval(nArrayList[2], env);
                                if (first.value is int) {
                                    return new node((int)first >= (int)second);
                                }
                                else if (first.value is long) {
                                    return new node((long)first >= (long)second);
                                }
                                else {
                                    return new node((double)first >= (double)second);
                                }
                            }
                        case paren.builtin.ANDAND: { // (&& X ..) short-circuit
                                for (int i = 1; i < nArrayList.Count; i++) {
                                    if (!(bool)eval(nArrayList[i], env)) {
                                        return node_false;
                                    }
                                }
                                return node_true;
                            }
                        case paren.builtin.OROR: { // (|| X ..) short-circuit
                                for (int i = 1; i < nArrayList.Count; i++) {
                                    if ((bool)eval(nArrayList[i], env)) {
                                        return node_true;
                                    }
                                }
                                return node_false;
                            }
                        case paren.builtin.NOT: { // (! X)
                            return new node(!((bool)eval(nArrayList[1], env)));
                            }
                        case paren.builtin.IF: { // (if CONDITION THEN_EXPR ELSE_EXPR)
                                node cond = nArrayList[1];
                                if ((bool)eval(cond, env)) {
                                    return eval(nArrayList[2], env);
                                }
                                else {
                                    return eval(nArrayList[3], env);
                                }
                            }
                        case paren.builtin.WHEN: { // (when CONDITION EXPR ..)
                                node cond = nArrayList[1];
                                if ((bool)eval(cond, env)) {
                                    int len = nArrayList.Count;
                                    for (int i = 2; i < len - 1; i++) {
                                        eval(nArrayList[i], env);
                                    }
                                    return eval(nArrayList[len - 1], env); // returns last EXPR
                                }
                                return node_null;
                            }
                        case paren.builtin.FOR: { // (for SYMBOL START END STEP EXPR ..)
                            node start = eval(nArrayList[2], env);
                                int len = nArrayList.Count;
                                if (start.value is int) {
                                    int last = (int)eval(nArrayList[3], env);
                                    int step = (int)eval(nArrayList[4], env);
                                    int a = (int)start;
                                    //node na = nArrayList[1];
                                    //node na = env.set(nArrayList[1].ToString(), new node(a));
                                    node na = env.set(((symbol)nArrayList[1].value).code, new node(a));
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            na.value = a;
                                            for (int i = 5; i < len; i++) {
                                                eval(nArrayList[i], env);
                                            }
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            na.value = a;
                                            for (int i = 5; i < len; i++) {
                                                eval(nArrayList[i], env);
                                            }
                                        }
                                    }
                                }
                                else if (start.value is long) {
                                    long last = (long)eval(nArrayList[3], env);
                                    long step = (long)eval(nArrayList[4], env);
                                    long a = (long)start;
                                    //node na = env.set(nArrayList[1].ToString(), new node(a));
                                    node na = env.set(((symbol)nArrayList[1].value).code, new node(a));
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            na.value = a;
                                            for (int i = 5; i < len; i++) {
                                                eval(nArrayList[i], env);
                                            }
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            na.value = a;
                                            for (int i = 5; i < len; i++) {
                                                eval(nArrayList[i], env);
                                            }
                                        }
                                    }
                                }
                                else {
                                    double last = (double)eval(nArrayList[3], env);
                                    double step = (double)eval(nArrayList[4], env);
                                    double a = (double)start;
                                    //node na = env.set(nArrayList[1].ToString(), new node(a));
                                    node na = env.set(((symbol)nArrayList[1].value).code, new node(a));
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            na.value = a;
                                            for (int i = 5; i < len; i++) {
                                                eval(nArrayList[i], env);
                                            }
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            na.value = a;
                                            for (int i = 5; i < len; i++) {
                                                eval(nArrayList[i], env);
                                            }
                                        }
                                    }
                                }
                                return node_null;
                            }
                        case paren.builtin.WHILE: { // (while CONDITION EXPR ..)
                                node cond = nArrayList[1];
                                int len = nArrayList.Count;
                                while ((bool)eval(cond, env)) {
                                    for (int i = 2; i < len; i++) {
                                        eval(nArrayList[i], env);
                                    }
                                }
                                return node_null;
                            }
                        case paren.builtin.STRLEN: { // (strlen X)
                            return new node(eval(nArrayList[1], env).stringValue().Length);
                            }
                        case paren.builtin.STRCAT: { // (strcat X ..)
                                int len = nArrayList.Count;
                                if (len <= 1) {
                                    return new node("");
                                }
                                node first = eval(nArrayList[1], env);
                                string acc = first.stringValue();
                                for (int i = 2; i < nArrayList.Count; i++) {
                                    acc += eval(nArrayList[i], env).stringValue();
                                }
                                return new node(acc);
                            }
                        case paren.builtin.CHAR_AT: { // (char-at X POSITION)
                            return new node((int)eval(nArrayList[1], env).stringValue()[(int)eval(nArrayList[2], env)]);
                            }
                        case paren.builtin.CHR: { // (chr X)
                                char[] temp = new char[] { '\0' };
                                temp[0] = (char)eval(nArrayList[1], env);
                                return new node(new string(temp));
                            }
                        case paren.builtin.STRING: { // (string X)
                            return new node(eval(nArrayList[1], env).stringValue());
                            }
                        case paren.builtin.DOUBLE: { // (double X)
                            return new node((double)eval(nArrayList[1], env));
                            }
                        case paren.builtin.INT: { // (int X)
                            return new node((int)eval(nArrayList[1], env));
                            }
                        case paren.builtin.LONG: { // (long X)
                            return new node((long)eval(nArrayList[1], env));
                            }
                        case paren.builtin.READ_STRING: { // (read-string X)
                            return new node(parse(eval(nArrayList[1], env).stringValue())[0].value);
                            }
                        case paren.builtin.TYPE: { // (type X)
                            return new node(eval(nArrayList[1], env).type());
                            }
                        case paren.builtin.EVAL: { // (eval X)
                                return eval(eval(nArrayList[1], env), env);
                            }
                        case paren.builtin.QUOTE: { // (quote X)
                                return nArrayList[1];
                            }
                        case paren.builtin.FN: { // (fn (ARGUMENT ..) BODY) => lexical closure
                            // anonymous function. lexical scoping
                            // (fn (ARGUMENT ..) BODY ..)                    
                            List<node> r = new List<node>();
                            r.Add(func);

                            for (int i = 1; i < nArrayList.Count; i++) {
                                r.Add(nArrayList[i]);
                            }
                            return new node(new fn(r, env));
                        }  
                        case paren.builtin.LIST: { // (list X ..)
                                List<node> ret = new List<node>();
                                for (int i = 1; i < nArrayList.Count; i++) {
                                    ret.Add(eval(nArrayList[i], env));
                                }
                                return new node(ret);
                            }
                        case paren.builtin.APPLY: { // (apply FUNC LIST)
                                List<node> expr = new List<node>();
                                node f = eval(nArrayList[1], env);
                                expr.Add(f);
                                List<node> lst = eval(nArrayList[2], env).arrayListValue();
                                for (int i = 0; i < lst.Count; i++) {
                                    List<node> item = new List<node>();
                                    item.Add(new node(new symbol("quote")));
                                    item.Add(lst[i]);
                                    expr.Add(new node(item));
                                }
                                return eval(new node(expr), env);
                            }
                        case paren.builtin.FOLD: { // (fold FUNC LIST)
                            node f = eval(nArrayList[1], env);
                            List<node> lst = eval(nArrayList[2], env).arrayListValue();
                            node acc = eval(lst[0], env);
                                List<node> expr = new List<node>(); // (FUNC ITEM)
                                expr.Add(f);
                                expr.Add(null); // first argument
                                expr.Add(null); // second argument
                                for (int i = 1; i < lst.Count; i++) {
                                    expr[1] = quote(acc);
                                    expr[2] = quote(lst[i]);
                                    acc = eval(new node(expr), env);
                                }
                                return acc;
                            }
                        case paren.builtin.MAP: { // (map FUNC LIST)
                            node f = eval(nArrayList[1], env);
                            List<node> lst = eval(nArrayList[2], env).arrayListValue();
                                List<node> acc = new List<node>();
                                List<node> expr = new List<node>(); // (FUNC ITEM)
                                expr.Add(f);
                                expr.Add(null);
                                for (int i = 0; i < lst.Count; i++) {
                                    expr[1] = quote(lst[i]);
                                    acc.Add(eval(new node(expr), env));
                                }
                                return new node(acc);
                            }
                        case paren.builtin.FILTER: { // (filter FUNC LIST)
                            node f = eval(nArrayList[1], env);
                            List<node> lst = eval(nArrayList[2], env).arrayListValue();
                                List<node> acc = new List<node>();
                                List<node> expr = new List<node>(); // (FUNC ITEM)
                                expr.Add(f);
                                expr.Add(null);
                                for (int i = 0; i < lst.Count; i++) {
                                    node item = quote(lst[i]);
                                    expr[1] = item;
                                    node ret = eval(new node(expr), env);
                                    if ((bool)ret) {
                                        acc.Add(item);
                                    }
                                }
                                return new node(acc);
                            }
                        case paren.builtin.RANGE: { // (range START END STEP)
                            node start = eval(nArrayList[1], env);
                                List<node> ret = new List<node>();
                                if (start.value is int) {
                                    int a = (int)eval(nArrayList[1], env);
                                    int last = (int)eval(nArrayList[2], env);
                                    int step = (int)eval(nArrayList[3], env);
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            ret.Add(new node(a));
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            ret.Add(new node(a));
                                        }
                                    }
                                }
                                else if (start.value is long) {
                                    long a = (long)eval(nArrayList[1], env);
                                    long last = (long)eval(nArrayList[2], env);
                                    long step = (long)eval(nArrayList[3], env);
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            ret.Add(new node(a));
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            ret.Add(new node(a));
                                        }
                                    }
                                }
                                else {
                                    double a = (double)eval(nArrayList[1], env);
                                    double last = (double)eval(nArrayList[2], env);
                                    double step = (double)eval(nArrayList[3], env);
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            ret.Add(new node(a));
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            ret.Add(new node(a));
                                        }
                                    }
                                }
                                return new node(ret);
                            }
                        case paren.builtin.NTH: { // (nth INDEX LIST)
                            int i = (int)eval(nArrayList[1], env);
                            List<node> lst = eval(nArrayList[2], env).arrayListValue();
                                return lst[i];
                            }
                        case paren.builtin.LENGTH: { // (length LIST)
                            List<node> lst = eval(nArrayList[1], env).arrayListValue();
                                return new node(lst.Count);
                            }
                        case paren.builtin.BEGIN: { // (begin X ..)
                                int last = nArrayList.Count - 1;
                                if (last <= 0) {
                                    return node_null;
                                }
                                for (int i = 1; i < last; i++) {
                                    eval(nArrayList[i], env);
                                }
                                return eval(nArrayList[last], env);
                            }
                        case paren.builtin.DOT: {
                                // Host interoperability
                                // (. CLASS METHOD ARGUMENT ..) ; Host method invocation
                                try {
                                    Type cls;
                                    object obj = null;
                                    string className = nArrayList[1].stringValue();                                    
                                    //if (nArrayList[1].value is symbol) { // class's static method e.g. (. System.Math Floor 1.5)
                                    if (nArrayList[1].value is symbol && env.get(((symbol)nArrayList[1].value).code) == null) { // class's static method e.g. (. System.Math Floor 1.5)
                                        cls = Type.GetType(className);
                                    } // object's method e.g. (. "abc" length)
                                    else {
                                        obj = eval(nArrayList[1], env).value;
                                        cls = obj.GetType();
                                    }
                                    Type[] parameterTypes = new Type[nArrayList.Count - 3];
                                    List<object> parameters = new List<object>();
                                    int last = nArrayList.Count - 1;
                                    for (int i = 3; i <= last; i++) {
                                        node a = eval(nArrayList[i], env);
                                        object param = a.value;
                                        parameters.Add(param);
                                        Type paramClass;
                                        if (a.clazz == null) {
                                            /*
                                            if (param is int) {
                                                paramClass = typeof(int);
                                            }
                                            else if (param is double) {
                                                paramClass = typeof(double);
                                            }
                                            else if (param is long) {
                                                paramClass = typeof(long);
                                            }
                                            else if (param is bool?) {
                                                paramClass = typeof(bool);
                                            }
                                            else {*/
                                                paramClass = param.GetType();
                                            //}
                                        }
                                        else {
                                            paramClass = a.clazz; // use hint
                                        }
                                        parameterTypes[i - 3] = paramClass;
                                    }
                                    string methodName = nArrayList[2].stringValue();
                                    MethodInfo method = cls.GetMethod(methodName, parameterTypes);
                                    return new node(method.Invoke(obj, parameters.ToArray()));
                                }
                                catch (Exception e) {
                                    Console.WriteLine(e.ToString());
                                    Console.WriteLine(e.StackTrace);
                                    return node_null;
                                }
                            }
                        case paren.builtin.DOTGET: {
                                // Host interoperability
                                // (.get CLASS FIELD) ; get Host field or property
                                try {
                                    Type cls;
                                    object obj = null;
                                    string className = nArrayList[1].stringValue();
                                    //if (nArrayList[1].value is symbol) { // class's static field e.g. (.get System.Math PI)
                                    if (nArrayList[1].value is symbol && env.get(((symbol)nArrayList[1].value).code) == null) { // class's static method e.g. (. System.Math Floor 1.5)
                                        cls = Type.GetType(className);
                                    } // object's method. e.g. (.get "abc" Length)
                                    else {
                                        obj = eval(nArrayList[1], env).value;
                                        cls = obj.GetType();
                                    }
                                    string fieldName = nArrayList[2].stringValue();
                                    System.Reflection.FieldInfo field = cls.GetField(fieldName);
                                    if (field == null) {                                        
                                        return new node(cls.GetProperty(fieldName).GetValue(obj, null));
                                    }                                    
                                    return new node(field.GetValue(cls));
                                }
                                catch (Exception e) {
                                    Console.WriteLine(e.ToString());
                                    Console.WriteLine(e.StackTrace);
                                    return node_null;
                                }
                            }
                        case paren.builtin.DOTSET: {
                                // Host interoperability
                                // (.set CLASS FIELD VALUE) ; set Host field or property
                                try {
                                    Type cls;
                                    object obj = null;
                                    string className = nArrayList[1].stringValue();
                                    //if (nArrayList[1].value is symbol) { // class's static field e.g. (.get System.Math PI)
                                    if (nArrayList[1].value is symbol && env.get(((symbol)nArrayList[1].value).code) == null) { // class's static method e.g. (. System.Math Floor 1.5)
                                        cls = Type.GetType(className);
                                    } // object's method
                                    else {
                                        obj = eval(nArrayList[1], env).value;
                                        cls = obj.GetType();
                                    }
                                    string fieldName = nArrayList[2].stringValue();
                                    object value = eval(nArrayList[3], env).value;
                                    System.Reflection.FieldInfo field = cls.GetField(fieldName);
                                    if (field == null) {
                                        cls.GetProperty(fieldName).SetValue(obj, value, null);
                                    }
                                    else {
                                        field.SetValue(cls, value);
                                    }
                                    return node_null;
                                }
                                catch (Exception e) {
                                    Console.WriteLine(e.ToString());
                                    Console.WriteLine(e.StackTrace);
                                    return node_null;
                                }
                            }
                        case paren.builtin.NEW: {
                                // Host interoperability
                                // (new CLASS ARG ..) ; create new Host object
                                try {
                                    string className = nArrayList[1].stringValue();                                    
                                    Type cls = Type.GetType(className);
                                    if (cls == null) {
                                        Console.Error.WriteLine(className + ": No such class");
                                        return node_null;
                                    }                                    
                                    Type[] parameterTypes = new Type[nArrayList.Count - 2];
                                    var parameters = new object[nArrayList.Count - 2];
                                    int last = nArrayList.Count - 1;
                                    for (int i = 2; i <= last; i++) {
                                        node a = eval(nArrayList[i], env);
                                        object param = a.value;                                        
                                        parameters[i - 2] = param;
                                        Type paramClass;
                                        if (a.clazz == null) {
                                            paramClass = param.GetType();
                                            Console.WriteLine(paramClass);
                                        }
                                        else
                                            paramClass = a.clazz; // use hint
                                        parameterTypes[i - 2] = paramClass;
                                    }                                    
                                    ConstructorInfo ctor = cls.GetConstructor(parameterTypes);                                                                        
                                    return new node(ctor.Invoke(parameters));
                                }
                                catch (Exception e) {
                                    Console.WriteLine(e.ToString());
                                    Console.WriteLine(e.StackTrace);
                                    return node_null;
                                }
                            }
                        case paren.builtin.PR: { // (pr X ..)
                                for (int i = 1; i < nArrayList.Count; i++) {
                                    if (i != 1) {
                                        Console.Write(" ");
                                    }
                                    Console.Write(eval(nArrayList[i], env).stringValue());
                                }
                                return node_null;
                            }
                        case paren.builtin.PRN: { // (prn X ..)
                                for (int i = 1; i < nArrayList.Count; i++) {
                                    if (i != 1) {
                                        Console.Write(" ");
                                    }
                                    Console.Write(eval(nArrayList[i], env).stringValue());
                                }
                                Console.WriteLine();
                                return node_null;
                            }
                        case paren.builtin.EXIT: { // (exit X)
                                Console.WriteLine();
                                Environment.Exit((int)eval(nArrayList[1], env));
                                return node_null;
                            }
                        case paren.builtin.SYSTEM: { // (system "notepad" "a.txt") ; run external program
                                List<string> args = new List<string>();
                                for (int i = 1; i < nArrayList.Count; i++) {
                                    args.Add(eval(nArrayList[i], env).stringValue());
                                }
                                var psi = System.Diagnostics.Process.Start(args[0], string.Join(" ", args.Skip(1)));
                                psi.WaitForExit();
                                return node_null;
                            }
                        case paren.builtin.CONS: { // (cons X LST): Returns a new list where x is the first element and lst is the rest.
                                //node x = new node(eval(nArrayList.get(1)).value);
                            node x = eval(nArrayList[1], env);
                            List<node> lst = eval(nArrayList[2], env).arrayListValue();
                                List<node> r = new List<node>();
                                r.Add(x);
                                foreach (node n2 in lst) {
                                    r.Add(n2);
                                }
                                return new node(r);
                            }
                        case paren.builtin.NULLP: { // (null? X): Returns true if X is null.
                            return new node(eval(nArrayList[1], env).value == null);
                            }
                        case paren.builtin.CAST: { // (cast CLASS X): Returns type-hinted object.
                            node x = eval(nArrayList[2], env);
                                try {
                                    x.clazz = Type.GetType(nArrayList[1].stringValue());
                                }
                                catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    Console.WriteLine(e.ToString());
                                    Console.WriteLine(e.StackTrace);
                                }
                                return x;
                            }
                        case paren.builtin.READ_LINE: { // (read-line)
                            return new node(Console.ReadLine());
                        }
                        case paren.builtin.SLURP: { // (slurp FILENAME): extracts characters from fileName
					        string filename = eval(nArrayList[1], env).stringValue();
                            return new node(System.IO.File.ReadAllText(filename));
                        }
                        case paren.builtin.SPIT: { // (spit FILENAME STRING): Opposite of slurp. Writes str to fileName.
                	        string filename = eval(nArrayList[1], env).stringValue();
                	        string str = eval(nArrayList[2], env).stringValue();
                            System.IO.File.WriteAllText(filename, str);
                            return new node(str.Length);
                        }
                        case paren.builtin.THREAD: { // (thread EXPR ..): Creates new thread and starts it.
                            var t = new Thread(() => {
                                for (int i = 1; i < nArrayList.Count; i++) eval(nArrayList[i], env);
                            });
                            t.Start();
                            return new node(t);
                        }
                        default: {
                                Console.Error.WriteLine("Not implemented function: [" + func.value.ToString() + "]");
                                return node_null;
                            }
                    } // end switch(found)
                }
                else {
                    if (func.value is fn) {
                        // anonymous function application. lexical scoping
                        // (fn (ARGUMENT ..) BODY ..)                	                	
                        fn f = (fn)func.value;
                        List<node> arg_syms = f.def[1].arrayListValue();
                        environment local_env = new environment(f.env);

                        int len = arg_syms.Count;
                        for (int i = 0; i < len; i++) { // assign arguments
                            //String k = arg_syms[i].stringValue();
                            node k = arg_syms[i];
                            node n2 = eval(nArrayList[i + 1], env);
                            //local_env.env[k] = n2;
                            local_env.env[((symbol)k.value).code] = n2;
                        }

                        len = f.def.Count;
                        node ret = null;
                        for (int i = 2; i < len; i++) { // body
                            ret = eval(f.def[i], local_env);
                        }
                        return ret;
                    }
                    else {
                        Console.Error.WriteLine("Unknown function: [" + func.value.ToString() + "]");
                        return node_null;
                    }
                }
            }
            else {
                //return (node)n.Clone();
                return n;
            }
        }

        internal List<node> compile_all(List<node> lst) {
            List<node> compiled = new List<node>();
            int last = lst.Count - 1;
            for (int i = 0; i <= last; i++) {
                compiled.Add(compile(lst[i]));
            }
            return compiled;
        }
        internal node eval_all(List<node> lst) {
            int last = lst.Count - 1;
            if (last < 0) {
                return node_null;
            }
            node ret = null;
            for (int i = 0; i <= last; i++) {
                ret = eval(lst[i], global_env);
            }
            return ret;
        }
        internal node eval_string(string s) {
            try {
                List<node> compiled = compile_all(parse(s));
                return eval_all(compiled);
            }
            catch (Exception e) {
                Console.Error.WriteLine(e);
                Console.Error.WriteLine(e.StackTrace);
                return node_null;
            }
        }
        internal void eval_print(string s) {
            Console.WriteLine(eval_string(s).stringValue());
        }
        internal static void prompt() {
            Console.Write("> ");
        }
        internal static void prompt2() {
            Console.Write("  ");
        }

        // read-eval-print loop
        internal void repl() {
            string code = "";
            while (true) {
                try {
                    if (code.Length == 0) {
                        prompt();
                    }
                    else {
                        prompt2();
                    }
                    string line = Console.ReadLine();
                    if (line == null) { // EOF
                        eval_print(code);
                        break;
                    }
                    code += "\n" + line;
                    tokenizer t = new tokenizer(code);
                    t.tokenize();
                    if (t.unclosed <= 0) { // no unmatched parenthesis nor quotation
                        eval_print(code);
                        code = "";
                    }
                }
                catch (Exception e) {
                    Console.WriteLine(e.ToString());
                    Console.WriteLine(e.StackTrace);
                    code = "";
                }
            }
        }
    }
}
