# Paren#: The Paren Programming Language written in C# #

(C) 2013-2015 Kim, Taegyoon

Paren is a dialect of Lisp. It is designed to be an embedded language. You can use .Net in your Paren program.

## Run ##
```
Usage: parensharp [OPTIONS...] [FILES...]

OPTIONS:
    -h    print this screen.
    -v    print version.
```

## Reference ##
```
Predefined Symbols:
 - -- ! != % && * . .get .set / ^ || + ++ < <= = == > >= apply begin cast ceil char-at chr cons dec def defmacro double E eval exit false filter floor fn fold for if inc int length list ln log10 long map new nth null null? PI pr prn quote rand range read-line read-string set slurp spit sqrt strcat string strlen system thread true type when while
Macros:
 defn join setfn
```

## Files ##
* paren.cs: Paren language library
* parensharp.cs: Paren REPL executable

## Examples ##
### Hello, World! ###
```
(prn "Hello, World!")
```

### Comment ###
```
#!/usr/bin/ParenSharp.exe
; comment
```

### Function ###

In a function, [lexical scoping](http://en.wikipedia.org/wiki/Lexical_scoping#Lexical_scoping) is used.

```
> ((fn (x y) (+ x y)) 1 2)
3
> ((fn (x) (* x 2)) 3)
6
> (setfn sum (x y) (+ x y))
(FN (x y) (+ x y))
> (sum 1 2)
3
> (fold sum (range 1 10 1))
55
> (set even? (fn (x) (== 0 (% x 2))))
(FN (x) (== 0 (% x 2)))
> (even? 3)
False
> (even? 4)
True
> (apply + (list 1 2 3))
6
> (map sqrt (list 1 2 3 4))
(1 1.4142135623731 1.73205080756888 2)
> (filter even? (list 1 2 3 4 5))
(2 4)
> (= "abc" "abc") ; Object.equals()
True
> (set x 1)
  ((fn (x) (prn x) (set x 3) (prn x)) 4) ; lexical scoping
  x    
4
3
1
> (set adder (fn (amount) (fn (x) (+ x amount)))) ; lexical scoping
  (set add3 (adder 3))
  (add3 4)
7
> (cons 1 (list 2 3))
(1 2 3)
```

#### Recursion ####
```
> (set factorial (fn (x) (if (<= x 1) x (* x (factorial (dec x))))))
  (for i 1 5 1 (prn i (factorial i)))
1 1
2 2
3 6
4 24
5 120
```

### List ###
```
> (nth 1 (list 2 4 6))
4
> (length (list 1 2 3))
3
```

### Macro ###
```
> (defmacro infix (a op ...) (op a ...)) (infix 3 + 4 5)
12
```

### Thread ###
```
> (set t1 (thread (for i 1 10 1 (pr "" i)))) (set t2 (thread (for j 11 20 1 (pr "" j)))) (join t1) (join t2)
 1 2 3 4 5 6 11 12 13 14 15 16 17 18 7 8 9 10 19 20
```

### .Net interoperability (from Paren) ###
```
> (. (new System.Random) NextDouble)
0.180564465085307 : System.Double
> (. System.Math Floor 1.5)
1
> (.get "abc" Length) ; object's property
3
> (. true ToString)
True
> (set i 3)
3
> (. System.Convert ToDouble i)
3
> (type (. System.Convert ToDouble i))
System.Double
> (.get System.Math PI) ; get field
3.14159265358979
> (.get ParenSharp.parensharp testField)

> (type (.get ParenSharp.parensharp testField))
null
> (.set ParenSharp.parensharp testField 1) ; set field
  (.get ParenSharp.parensharp testField)
1
> (.set ParenSharp.parensharp testField "abc")
  (.get ParenSharp.parensharp testField)
abc
```

### .Net interoperability (from C#) ###

parenTest.cs
```
namespace test {
    public class player {
        private int life;
        public virtual int getLife() {
            return life;
        }
        public virtual void setLife(int life) {
            this.life = life;
        }
    }

    public class parenTest {
        public static object testField;        
        public parenTest() {
            var p = new ParenSharp.paren();
            player pl = new player();

            // Method 1: using class's field
            testField = pl;
            p.eval_string("(set pl (.get test.parenTest testField))");
            p.eval_string("(. pl setLife 100)");
            System.Console.WriteLine((int)p.eval_string("(. pl getLife)"));

            // Method 2: not using class's field, set variable to C#'s local variable            
            p.global_env.env.Add("pl2", new ParenSharp.paren.node(pl));
            p.eval_string("(. pl2 setLife 200)");
            System.Console.WriteLine((int)p.eval_string("(. pl2 getLife)"));
            p.global_env.env.Remove("p12"); // remove variable
        }
    }
}

```

### System Command ###
```
(system "notepad" "a.txt")
```

[Project Euler solutions in Paren](https://bitbucket.org/ktg/euler-paren)

### [99 Bottles of Beer](http://en.wikipedia.org/wiki/99_Bottles_of_Beer) ###
```
(for i 99 1 -1
  (prn i "bottles of beer on the wall," i "bottles of beer.")
  (prn "Take one down and pass it around," (dec i) "bottle of beer on the wall."))
```

## Alternative Implementations ##
* [Paren](https://bitbucket.org/ktg/paren) (Paren running natively)
* [Parenj](https://bitbucket.org/ktg/parenj) (Paren running on the Java Virtual Machine)
* [Parenjs](https://bitbucket.org/ktg/parenjs) (Paren compiler targeting JavaScript)
* [Paren#](https://bitbucket.org/ktg/parensharp) (Paren running on the .Net Framework)

## License ##

   Copyright 2013-2015 Kim, Taegyoon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
