﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParenSharp
{
    class parensharp
    {
        public static object testField = null;
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                paren p = new paren();
                p.print_logo();
                p.repl();
                Console.WriteLine();
                return;
            }
            else if (args.Length == 1)
            {
                if (args[0].Equals("-h"))
                {
                    Console.WriteLine("Usage: parensharp [OPTIONS...] [FILES...]");
                    Console.WriteLine();
                    Console.WriteLine("OPTIONS:");
                    Console.WriteLine("    -h    print this screen.");
                    Console.WriteLine("    -v    print version.");
                    return;
                }
                else if (args[0].Equals("-v"))
                {
                    Console.WriteLine(paren.VERSION);
                    return;
                }
            }

            // execute files, one by one
            foreach (string fileName in args)
            {
                paren p = new paren();
                try
                {
                    string code = System.IO.File.ReadAllText(fileName);
                    p.eval_string(code);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    Console.WriteLine(e.StackTrace);
                }
            }
        }
    }
}
